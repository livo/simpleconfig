package simpleconfig

import (
	"encoding/json"
	"testing"
)

type Config struct {
	Example string `mapstructure:"example" json:"example" toml:"example"`
}

func TestLoadByDefaultFile(t *testing.T) {
	var cfg = &Config{}
	LoadByDefaultFile(cfg)
	str, err := json.Marshal(cfg)
	println(string(str), err)
}

func TestLoadFromFileByEnv(t *testing.T) {

}

func TestLoadFromFile(t *testing.T) {

}
